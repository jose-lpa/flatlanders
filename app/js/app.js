(function() {
    var app = angular.module('store', []);

    app.controller('StoreController', function() {
        this.products = gems;
    });

    app.controller('ReviewController', function() {
        this.review = {};

        this.addReview = function(product) {
            product.reviews.push(this.review);
            this.review = {};
        };
    });

    app.directive('productTitle', function() {
        // Defines the configuration of the custom directive.
        return {
            restrict: 'E',  // Type of directive ('E' is for 'Element').
            templateUrl: 'product_title.html'  // URL of a template.
        };
    });

    app.directive('productPanels', function() {
        return {
            restrict: 'E',
            templateUrl: 'product_panels.html',
            controller: function() {
                this.tab = 1;

                this.selectTab = function(setTab) { 
                    this.tab = setTab;
                };

                this.isSelected = function(checkTab) {
                    return this.tab == checkTab;
                };
            },
            controllerAs: 'panel'
        };
    });

    var gems = [
        {
            name: 'Azurite',
            description: "Some gems have hidden qualities beyond their luster, beyond their shine... Azurite is one of those gems.",
            shine: 8,
            price: 110.50,
            rarity: 7,
            color: '#CCC',
            faces: 14,
            images: [
                "images/gem-0.png",
                "images/gem-1.png",
                "images/gem-2.png"
            ],
            reviews: [
                {
                    stars: 5,
                    body: 'I love this product!',
                    author: 'joe@thomas.com'
                },
                {
                    stars: 4,
                    body: 'Beautiful shape.',
                    author: 'martha@sharklasers.com'
                },
                {
                    stars: 1,
                    body: "I don't like it at all.",
                    author: 'jenny@assholes.net'
                },
            ]
        }, 
        {
            name: 'Bloodstone',
            description: "Origin of the Bloodstone is unknown, hence its low value. It has a very high shine and 12 sides, however.",
            shine: 9,
            price: 22.90,
            rarity: 6,
            color: '#EEE',
            faces: 12,
            images: [
                "images/gem-3.png",
                "images/gem-4.png",
                "images/gem-5.png"
            ]
        }, 
        {
            name: 'Zircon',
            description: "Zircon is our most coveted and sought after gem. You will pay much to be the proud owner of this gorgeous and high shine gem.",
            shine: 70,
            price: 1100,
            rarity: 2,
            color: '#000',
            faces: 6,
            images: [
                "images/gem-6.png",
                "images/gem-7.png",
                "images/gem-8.png"
            ]
        }
    ];
})();
